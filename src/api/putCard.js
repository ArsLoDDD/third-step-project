import instance from './instance.js'

const putCard = (cardId, card) => {
	return instance.put(`/${cardId}`, card)
}
export default putCard
