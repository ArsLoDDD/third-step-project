import instance from './instance.js'

const loginFun = obj => {
	try {
		return instance
			.post('/login', obj, {
				headers: {
					'Content-Type': 'application/json',
				},
			})
			.then(({ data, status }) => {
				if (status === 200) {
					if (localStorage.getItem('token')) {
						localStorage.removeItem('token')
					}
					localStorage.setItem('token', data)
					return true
				}
			})
	} catch (error) {
		console.log(error)
	}
}

export default loginFun
