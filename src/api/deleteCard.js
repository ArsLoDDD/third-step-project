import instance from './instance.js'

const deleteCard = id => {
	try {
		const res = instance.delete(`/${id}`)
		return res
	} catch (error) {
		console.log(error)
	}
}

export default deleteCard
