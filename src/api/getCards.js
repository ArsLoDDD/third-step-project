import instance from './instance.js'

const getCards = async () => {
	if (!localStorage.getItem('token')) {
		return
	}
	try {
		const data = await instance.get('/').then(res => {
			return res.data
		})
		return data
	} catch (error) {
		console.log(error)
	}
}

export default getCards
