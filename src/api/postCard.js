import instance from './instance.js'

async function postCard(card) {
	try {
		console.log(card);
		return await instance.post('/', card)
	} catch (error) {
		console.error(error)
	}
}

export default postCard
