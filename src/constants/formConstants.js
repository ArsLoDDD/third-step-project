const loginBtn = document.querySelector('.signIn')
const logOut = document.querySelector('.logOut')
const addNewVisit = document.querySelector('.add-new-card-visit')

export { loginBtn, logOut, addNewVisit }
