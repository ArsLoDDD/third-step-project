// header

// login form

const authorization = 'Authorization'
const loginBtnContent = 'Sign In'
const logOutBtnContent = 'Log Out'
const passwordPlaceholder = 'Enter your password'
const emailPlaceholder = 'Enter your email'
export {
	authorization,
	loginBtnContent,
	passwordPlaceholder,
	emailPlaceholder,
	logOutBtnContent,
}
