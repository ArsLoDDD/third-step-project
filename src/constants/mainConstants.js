const searchContainer = document.querySelector('.search-container')
const main = document.querySelector('main')
const searchInput = document.querySelector('#search-input')
const prioritySelect = document.querySelector('#priority-select')
const statusSelect = document.querySelector('#status-select')
const clientCards = document.querySelector('.card__container')

export {
	searchContainer,
	main,
	searchInput,
	prioritySelect,
	statusSelect,
	clientCards,
}
