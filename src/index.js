import LoginForm from './classes/LoginForm.js'
import { loginBtn } from './constants/formConstants.js'
import getCards from './api/getCards.js'
import checkToken from './utils/checkToken.js'
import renderCards from './utils/renderCards.js'
import OnLogBtns from './classes/OnLogBtns.js'
import SignUp from './classes/SignUp.js'
import Empty from './classes/Empty.js'

////////
const cardContainer = document.querySelector('.card__container')
const priorityDropdown = document.getElementById('priority-select')
const searchInput = document.getElementById('search-input')
const statusDropdown = document.getElementById('status-select')
const searchContainer = document.querySelector('.search-container')
const signIn = document.querySelector('.signIn')
const signUp = document.querySelector('.signUp')

let cards
/////////

// логин
loginBtn.addEventListener('click', () => {
	const loginForm = new LoginForm()
	loginForm.render()
})
signUp.addEventListener('click', () => {
	const signUp = new SignUp()
	signUp.render()
})

if (checkToken()) {
	new OnLogBtns().render()
	signIn.classList.add('none')
	signUp.classList.add('none')
	searchContainer.style.display = 'flex'

	// получение всех карточек
	;(async () => {
		renderCards()
		if (cardContainer.children.length === 0) {
			new Empty().render()
		}
	})()
}

let debounceTimeoutId = null

const debounce = (func, delay) => {
	clearTimeout(debounceTimeoutId)
	debounceTimeoutId = setTimeout(func, delay)
}

const updateCardList = async () => {
	cards = await getCards()

	const obj = {
		status: statusDropdown?.value,
		priority: priorityDropdown?.value,
		search: searchInput?.value.toLowerCase(),
	}

	let filteredCards = cards.filter(card => {
		console.log(card);
		if (obj.status === 'all') {
			return true
		}
		if (obj.status.toLowerCase() === card.status?.toLowerCase()) {
			return true
		}
		return false
	})

	filteredCards = filteredCards.filter(card => {
		if (obj.priority === 'all') {
			return true
		}
		if (obj.priority.toLowerCase() === card.priority?.toLowerCase()) {
			return true
		}
		return false
	})

	filteredCards = filteredCards.filter(card => {
		if (obj.search === '') {
			return true
		}
		if (
			card.fullName.toLowerCase().includes(obj.search) ||
			card.purpose.toLowerCase().includes(obj.search) ||
			card.description.toLowerCase().includes(obj.search)
		) {
			console.log(card);
			return true
		}
		return false
	})

	cardContainer.innerHTML = ''

	renderCards(cardContainer, filteredCards)
}

statusDropdown.addEventListener('change', updateCardList)
priorityDropdown.addEventListener('change', updateCardList)
searchInput.addEventListener('input', () => debounce(updateCardList, 500))
