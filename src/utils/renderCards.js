import VisitCardiologist from '../classes/VisitCardiologist.js'
import VisitDentist from '../classes/VisitDentist.js'
import VisitTherapist from '../classes/VisitTherapist.js'
import getCards from '../api/getCards.js'
import EditForm from '../classes/EditForm.js'
import Empty from '../classes/Empty.js'

const cardsContainer = document.querySelector('.card__container')

let allCards = await getCards()
const renderCards = async (
	cardContainer = cardsContainer,
	cards = allCards
) => {
	if (cards.length === 0) {
		new Empty().render()
		return
	}
	await cards.forEach(
		({
			doctor,
			fullName,
			purpose,
			description,
			priority,
			pressure,
			weight,
			heartDisease,
			age,
			lastVisitDate,
			cardId,
			id,
			status,
		}) => {
			if (doctor === 'Cardiologist') {
				new VisitCardiologist(
					new EditForm(
						doctor,
						fullName,
						purpose,
						description,
						priority,
						pressure,
						weight,
						heartDisease,
						age,
						lastVisitDate,
						cardId,
						id,
						status
					),
					cardContainer,
					doctor,
					fullName,
					purpose,
					description,
					priority,
					pressure,
					weight,
					heartDisease,
					age,
					lastVisitDate,
					cardId,
					id,
					status
				).render()
			}
			if (doctor === 'Dentist') {
				new VisitDentist(
					new EditForm(
						doctor,
						fullName,
						purpose,
						description,
						priority,
						pressure,
						weight,
						heartDisease,
						age,
						lastVisitDate,
						cardId,
						id,
						status
					),
					cardContainer,
					doctor,
					fullName,
					purpose,
					description,
					priority,
					pressure,
					weight,
					heartDisease,
					age,
					lastVisitDate,
					cardId,
					id,
					status
				).render()
			}
			if (doctor === 'Therapist') {
				new VisitTherapist(
					new EditForm(
						doctor,
						fullName,
						purpose,
						description,
						priority,
						pressure,
						weight,
						heartDisease,
						age,
						lastVisitDate,
						cardId,
						id,
						status
					),
					cardContainer,
					doctor,
					fullName,
					purpose,
					description,
					priority,
					pressure,
					weight,
					heartDisease,
					age,
					lastVisitDate,
					cardId,
					id
				).render()
			}
		}
	)
}

export default renderCards
