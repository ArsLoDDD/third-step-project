import VisitCardiologist from './VisitCardiologist.js'
import VisitDentist from './VisitDentist.js'
import VisitTherapist from './VisitTherapist.js'
import { main } from '../constants/mainConstants.js'
import Form from './Form.js'
import postCard from '../api/postCard.js'
import EditForm from './EditForm.js'

export default class NewElementForm extends Form {
	constructor(modalOverlay, modalContainer, modalTitle, modalForm) {
		super(modalOverlay, modalContainer, modalTitle, modalForm)
		this.selectDoctor = document.createElement('select')
		this.selectPriority = document.createElement('select')
		this.selectText = document.createElement('p')
		this.selectPriorityBox = document.createElement('div')
		this.submitBtn = document.createElement('button')
		this.doctors = ['Choose a doctor', 'Cardiologist', 'Dentist', 'Therapist']
		this.fullName = document.createElement('input')
		this.description = document.createElement('input')
		this.purpose = document.createElement('input')
		this.pressure = document.createElement('input')
		this.weight = document.createElement('input')
		this.heartDisease = document.createElement('input')
		this.age = document.createElement('input')
		this.lastVisitDate = document.createElement('input')
		this.lastVisitBox = document.createElement('div')
		this.lastVisitText = document.createElement('p')
		this.infoBox = document.createElement('div')
		this.spinner = document.createElement('div')
		this.spinner.classList.add('spinner')
		this.cardContainer = document.querySelector('.card__container')
		this.haveBeen = document.querySelector('.item-have-been')
	}

	createElements() {
		super.createElements()
		this.modalContainer.classList.add('modal-new-element-container')
		this.modalOverlay.classList.add('modal-overlay')
		this.modalTitle.classList.add('modal-new-element-title')
		this.modalForm.classList.add('modal-new-element-form')
		this.selectDoctor.classList.add('modal-new-element-select')
		this.doctors.forEach(doctor => {
			const option = document.createElement('option')
			option.setAttribute('value', doctor)
			option.textContent = doctor
			this.selectDoctor.append(option)
		})

		this.submitBtn.classList.add('modal-new-element-submit-btn')
		this.modalTitle.textContent = 'New Visit'
		this.submitBtn.textContent = 'Create'
		this.modalForm.append(this.selectDoctor, this.infoBox, this.submitBtn)
		this.modalContainer.append(this.modalTitle, this.modalForm)
		this.modalOverlay.append(this.modalContainer)
		main.append(this.modalOverlay)
	}
	renderTherapistInputs() {
		const base = this.renderBaseInputs()
		this.age.setAttribute('type', 'text')
		this.age.setAttribute('id', 'age')
		this.age.setAttribute('placeholder', 'Age')
		this.infoBox.append(...base, this.age)
	}
	renderDentistInputs() {
		const base = this.renderBaseInputs()
		this.lastVisitDate.setAttribute('type', 'date')
		this.lastVisitDate.setAttribute('id', 'last-visit-date')
		this.lastVisitDate.setAttribute('lang', 'en')

		this.lastVisitText.classList.add('modal-new-element-last-visit-text')
		this.lastVisitText.textContent = 'Last Visit:'

		this.lastVisitBox.classList.add('modal-new-element-last-visit-box')
		this.lastVisitBox.append(this.lastVisitText, this.lastVisitDate)

		this.infoBox.append(...base, this.lastVisitBox)
	}
	renderCardiologistInputs() {
		const base = this.renderBaseInputs()

		this.pressure.setAttribute('type', 'text')
		this.pressure.setAttribute('id', 'blood-pressure')
		this.pressure.setAttribute(
			'placeholder',
			'Blood Pressure in format "120-90"'
		)

		this.weight.setAttribute('type', 'text')
		this.weight.setAttribute('id', 'weight')
		this.weight.setAttribute('placeholder', 'Weight')

		this.heartDisease.setAttribute('type', 'text')
		this.heartDisease.setAttribute('id', 'heart-disease')
		this.heartDisease.setAttribute('placeholder', 'Heart Disease')

		this.age.setAttribute('type', 'text')
		this.age.setAttribute('id', 'age')
		this.age.setAttribute('placeholder', 'Age')

		this.infoBox.append(
			...base,
			this.pressure,
			this.weight,
			this.heartDisease,
			this.age
		)
	}
	renderBaseInputs() {
		this.infoBox.classList.add('modal-new-element-info-box')
		this.selectPriorityBox.classList.add(
			'modal-new-element-select-priority-box'
		)
		this.selectText.classList.add('modal-new-element-select-text')
		this.selectText.textContent = 'Select priority:'
		this.selectPriority.classList.add('modal-new-element-select-priority')
		this.selectPriority.setAttribute('id', 'priority')
		this.selectPriorityBox.append(this.selectText, this.selectPriority)

		this.purpose.setAttribute('type', 'text')
		this.purpose.setAttribute('id', 'purpose')
		this.purpose.setAttribute('placeholder', 'Purpose')
		this.purpose.setAttribute('required', '')

		this.fullName.setAttribute('type', 'text')
		this.fullName.setAttribute('id', 'full-name')
		this.fullName.setAttribute('placeholder', 'Full Name')
		this.fullName.setAttribute('required', '')

		this.description = document.createElement('textarea')
		this.description.setAttribute('id', 'description')
		this.description.setAttribute('placeholder', 'Description')
		this.description.style.resize = 'none'
		this.description.style.width = '100%'
		this.description.style.height = '100px'

		this.selectPriority.setAttribute('id', 'priority')

		if (this.selectPriority.hasChildNodes()) {
			this.selectPriority.innerHTML = ''
		}
		const regularOption = document.createElement('option')
		regularOption.setAttribute('value', 'Regular')
		regularOption.textContent = 'Regular'
		this.selectPriority.appendChild(regularOption)

		const priorityOption = document.createElement('option')
		priorityOption.setAttribute('value', 'Priority')
		priorityOption.textContent = 'Priority'
		this.selectPriority.appendChild(priorityOption)

		const emergencyOption = document.createElement('option')
		emergencyOption.setAttribute('value', 'Emergency')
		emergencyOption.textContent = 'Emergency'
		this.selectPriority.appendChild(emergencyOption)

		return [
			this.fullName,
			this.purpose,
			this.description,
			this.selectPriorityBox,
		]
	}
	getFormValues() {
		const obj = {
			doctor: this.selectDoctor.value,
			fullName: this.fullName.value,
			description: this.description.value ? this.description.value : '-',
			purpose: this.purpose.value ? this.purpose.value : '-',
			priority: this.selectPriority.value,
			pressure: this.pressure.value ? this.pressure.value : '-',
			weight: this.weight.value ? this.weight.value : '-',
			heartDisease: this.heartDisease.value ? this.heartDisease.value : '-',
			age: this.age.value ? this.age.value : '-',
			lastVisitDate: this.lastVisitDate.value ? this.lastVisitDate.value : '-',
			cardId: this.cardId ? this.cardId : null,
			status: this.status ? this.status : 'Open',
		}
		return obj
	}
	addEventListeners() {
		super.addEventListeners()
		this.selectDoctor.addEventListener('change', e => {
			this.infoBox.innerHTML = ''
			this.renderInputs()
		})
		this.submitBtn.addEventListener('click', async e => {
			e.preventDefault()
			if (this.validate() === false) {
				return
			}
			if (this.selectDoctor.value === 'Choose a doctor') {
				return
			}
			const obj = this.getFormValues()
			this.submitBtn.remove()
			this.modalForm.append(this.spinner)
			const postCardSend = await postCard(obj)
			if (postCardSend.status === 200) {
				const {
					doctor,
					fullName,
					purpose,
					description,
					priority,
					pressure,
					weight,
					heartDisease,
					age,
					lastVisitDate,
					cardId,
					id,
					status,
				} = postCardSend.data
				if (postCardSend.data.doctor === 'Cardiologist') {
					const newVisit = new VisitCardiologist(
						new EditForm(
							doctor,
							fullName,
							purpose,
							description,
							priority,
							pressure,
							weight,
							heartDisease,
							age,
							lastVisitDate,
							cardId,
							id,
							status
						),
						this.cardContainer,
						doctor,
						fullName,
						purpose,
						description,
						priority,
						pressure,
						weight,
						heartDisease,
						age,
						lastVisitDate,
						cardId,
						id,
						status
					)
					newVisit.render()
				}
				if (postCardSend.data.doctor === 'Dentist') {
					new VisitDentist(
						new EditForm(
							doctor,
							fullName,
							purpose,
							description,
							priority,
							pressure,
							weight,
							heartDisease,
							age,
							lastVisitDate,
							cardId,
							id,
							status
						),
						this.cardContainer,
						doctor,
						fullName,
						purpose,
						description,
						priority,
						pressure,
						weight,
						heartDisease,
						age,
						lastVisitDate,
						cardId,
						id,
						status
					).render()
				}
				if (postCardSend.data.doctor === 'Therapist') {
					new VisitTherapist(
						new EditForm(
							doctor,
							fullName,
							purpose,
							description,
							priority,
							pressure,
							weight,
							heartDisease,
							age,
							lastVisitDate,
							cardId,
							id,
							status
						),
						this.cardContainer,
						doctor,
						fullName,
						purpose,
						description,
						priority,
						pressure,
						weight,
						heartDisease,
						age,
						lastVisitDate,
						cardId,
						id,
						status
					).render()
				}
			}
			if (this.cardContainer.children.length > 1) {
				this.haveBeen.remove()
			}
			this.closeModal()
		})
	}
	renderInputs() {
		if (this.selectDoctor.value === 'Cardiologist') {
			this.renderCardiologistInputs()
		}
		if (this.selectDoctor.value === 'Dentist') {
			this.renderDentistInputs()
		}
		if (this.selectDoctor.value === 'Therapist') {
			this.renderTherapistInputs()
		}
	}
	validate() {
		const letters = /^[a-zA-Zа-яА-ЯіІїЇєЄґҐ]+$/
		const numbers = /^[\d-]+$/
		const fullName = this.fullName.value.trim()
		const purpose = this.purpose.value.trim()
		const pressure = this.pressure.value
		const weight = this.weight.value
		const age = this.age.value

		const messageName = document.createElement('span')
		const messagePurpose = document.createElement('span')
		const messagePressure = document.createElement('span')
		const messageWeight = document.createElement('span')
		const messageAge = document.createElement('span')

		document.querySelectorAll('.invalid-input-message').forEach(el => {
			el.classList.add('none')
		})

		messageName.classList.add('invalid-input-message')
		messagePurpose.classList.add('invalid-input-message')
		messagePressure.classList.add('invalid-input-message')
		messageWeight.classList.add('invalid-input-message')
		messageAge.classList.add('invalid-input-message')

		this.fullName.classList.remove('invalid-input')
		this.purpose.classList.remove('invalid-input')
		this.pressure.classList.remove('invalid-input')
		this.weight.classList.remove('invalid-input')
		this.age.classList.remove('invalid-input')

		if (!letters.test(fullName)) {
			this.fullName.classList.add('invalid-input')
			messageName.innerText = 'Name must contain only letters'
			this.fullName.insertAdjacentElement('beforebegin', messageName)
		}

		if (fullName.length < 2) {
			this.fullName.classList.add('invalid-input')
			messageName.innerText = 'Name must be at least 2 characters long'
			this.fullName.insertAdjacentElement('beforebegin', messageName)
		}

		if (purpose.length < 5) {
			this.purpose.classList.add('invalid-input')
			messagePurpose.innerText = 'Purpose must be at least 5 characters long'
			this.purpose.insertAdjacentElement('beforebegin', messagePurpose)
		}

		if (!numbers.test(pressure) && !numbers === '') {
			this.pressure.classList.add('invalid-input')
			messagePressure.innerText =
				'Please input blood pressure in format "120-90"'
			this.pressure.insertAdjacentElement('beforebegin', messagePressure)
		}

		if ((weight != '' && 2 > weight) || weight > 250 || isNaN(weight)) {
			this.weight.classList.add('invalid-input')
			messageWeight.innerText = 'Please input correct weight'
			this.weight.insertAdjacentElement('beforebegin', messageWeight)
		}

		if ((age != '' && 0 > age) || age > 100 || isNaN(age)) {
			this.age.classList.add('invalid-input')
			messageAge.innerText = 'Please input correct age'
			this.age.insertAdjacentElement('beforebegin', messageAge)
		}

		if (document.querySelector('.invalid-input')) {
			return false
		}
	}
}
