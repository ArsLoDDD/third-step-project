import Visit from './Visit.js'

class VisitTherapist extends Visit {
	constructor(
		edit,
		appendElement,
		doctor,
		fullName,
		purpose,
		description,
		priority,
		pressure = null,
		weight = null,
		heartDisease = null,
		age,
		lastVisitDate = null,
		cardId,
		id,
		status
	) {
		super(
			edit,
			appendElement,
			doctor,
			fullName,
			purpose,
			description,
			priority,
			pressure,
			weight,
			heartDisease,
			age,
			lastVisitDate,
			cardId,
			id,
			status
		)
	}
	createElements() {
		super.createElements()
		this.specificInfo.innerHTML = `<p class="additional-info__age" id="client-age">Age: ${this.age}</p>`
	}
}

export default VisitTherapist
