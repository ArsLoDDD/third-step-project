import NewElementForm from '../classes/NewElementForm.js'
export default class OnLogBtns {
	constructor() {
		this.logOutBtn = document.createElement('button')
		this.addNew = document.createElement('button')
		this.btnBox = document.querySelector('.header__btn-box')
		this.cardContainer = document.querySelector('.card__container')
		this.signIn = document.querySelector('.signIn')
		this.signUp = document.querySelector('.signUp')
		this.searchContainer = document.querySelector('.search-container')
		this.statusSelect = document.getElementById('status-select')
		this.prioritySelect = document.getElementById('priority-select')
		this.searchInput = document.getElementById('search-input')
	}
	createElements() {
		this.logOutBtn.classList.add('login-btn')
		this.logOutBtn.classList.add('logOut')
		this.addNew.classList.add('add-new-card-visit')

		this.logOutBtn.textContent = 'Log Out'
		this.addNew.textContent = 'Add New Element'

		this.btnBox.append(this.logOutBtn, this.addNew)
	}
	addEventListeners() {
		this.logOutBtn.addEventListener('click', () => {
			localStorage.removeItem('token')
			this.cardContainer.innerHTML = ''
			this.logOutBtn.remove()
			this.addNew.remove()

			this.signIn.classList.remove('none')
			this.signUp.classList.remove('none')

			this.searchInput.value = ''
			this.statusSelect.value = 'all'
			this.prioritySelect.value = 'all'
			this.searchContainer.style.display = 'none'
		})
		this.addNew.addEventListener('click', () => {
			const newElementForm = new NewElementForm()
			newElementForm.render()
		})
	}
	render() {
		this.createElements()
		this.addEventListeners()
	}
}
