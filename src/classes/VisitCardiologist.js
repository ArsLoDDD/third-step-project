import Visit from './Visit.js'

class VisitCardiologist extends Visit {
	constructor(
		edit,
		appendElement,
		doctor,
		fullName,
		purpose,
		description,
		priority,
		pressure,
		weight,
		heartDisease,
		age,
		lastVisitDate = null,
		cardId,
		id,
		status
	) {
		super(
			edit,
			appendElement,
			doctor,
			fullName,
			purpose,
			description,
			priority,
			pressure,
			weight,
			heartDisease,
			age,
			lastVisitDate,
			cardId,
			id,
			status
		)
	}
	createElements() {
		super.createElements()
		this.specificInfo.innerHTML = `<p class="additional-info__pressure" id="client-pressure">Usual Pressure: ${this.pressure}</p>
    <p class="additional-info__weight" id="client-weight">BMI: ${this.weight}</p>
    <p class="additional-info__heart-disease" id="client-heart-disease">Cardiovascular Disease: ${this.heartDisease}</p>
    <p class="additional-info__age" id="client-age">Age: ${this.age}</p>`
	}
}

export default VisitCardiologist
