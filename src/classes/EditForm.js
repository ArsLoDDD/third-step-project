import { main } from '../constants/mainConstants.js'
import putCard from '../api/putCard.js'
import renderCards from '../utils/renderCards.js'
import getCards from '../api/getCards.js'
// Я знаю что можно было не копировать код, но у меня был баг который я не знал как пофиксить на тот момент, а когда я его пофиксил, то уже не хотелось удалять этот класс так как он был очень удобен
export default class EditForm {
	constructor(
		doctor,
		fullName,
		purpose,
		description,
		priority,
		pressure,
		weight,
		heartDisease,
		age,
		lastVisitDate,
		cardId,
		id,
		status = 'Open'
	) {
		this.doctor = doctor
		this.fullName = fullName
		this.purpose = purpose
		this.description = description
		this.priority = priority
		this.pressure = pressure
		this.weight = weight
		this.heartDisease = heartDisease
		this.age = age
		this.lastVisitDate = lastVisitDate
		this.cardId = cardId
		this.id = id
		this.status = status
		this.modalOverlay = document.createElement('div')
		this.modalContainer = document.createElement('div')
		this.modalTitle = document.createElement('h2')
		this.modalForm = document.createElement('form')
		this.selectPriorityInput = document.createElement('select')
		this.selectText = document.createElement('p')
		this.selectPriorityBox = document.createElement('div')
		this.submitBtn = document.createElement('button')
		this.fullNameInput = document.createElement('input')
		this.descriptionInput = document.createElement('input')
		this.purposeInput = document.createElement('input')
		this.pressureInput = document.createElement('input')
		this.weightInput = document.createElement('input')
		this.heartDiseaseInput = document.createElement('input')
		this.ageInput = document.createElement('input')
		this.lastVisitDateInput = document.createElement('input')
		this.lastVisitBox = document.createElement('div')
		this.lastVisitText = document.createElement('p')
		this.infoBox = document.createElement('div')
		this.spinner = document.createElement('div')
		this.spinner.classList.add('spinner')
		this.cardContainer = document.querySelector('.card__container')
		this.selectStatusInput = document.createElement('select')
		this.selectStatusText = document.createElement('p')
		this.selectStatusBox = document.createElement('div')
		this.statuses = ['Open', 'Done']
		this.descriptionInput = document.createElement('textarea')
		this.statusSelect = document.getElementById('status-select')
		this.prioritySelect = document.getElementById('priority-select')
		this.searchInput = document.getElementById('search-input')
	}

	createElements() {
		this.modalContainer.classList.add('modal-new-element-container')
		this.modalOverlay.classList.add('modal-overlay')
		this.modalTitle.classList.add('modal-new-element-title')
		this.modalForm.classList.add('modal-new-element-form')
		this.selectPriorityInput.classList.add('modal-new-element-select')
		this.submitBtn.classList.add('modal-new-element-submit-btn')

		this.modalTitle.textContent = 'Edit Visit'
		this.submitBtn.textContent = 'Save'

		this.modalForm.append(this.infoBox, this.submitBtn)
		this.modalContainer.append(this.modalTitle, this.modalForm)
		this.modalOverlay.append(this.modalContainer)
		main.append(this.modalOverlay)
	}
	renderTherapistInputs() {
		const base = this.renderBaseInputs()
		this.ageInput.setAttribute('type', 'text')
		this.ageInput.setAttribute('id', `age-${this.id}`)
		this.ageInput.setAttribute('placeholder', 'Age')
		this.infoBox.append(...base, this.ageInput)
	}
	renderDentistInputs() {
		const base = this.renderBaseInputs()
		this.lastVisitDateInput.setAttribute('type', 'date')
		this.lastVisitDateInput.setAttribute('id', `last-visit-date-${this.id}`)
		this.lastVisitDateInput.setAttribute('lang', 'en')

		this.lastVisitText.classList.add('modal-new-element-last-visit-text')
		this.lastVisitText.textContent = 'Last Visit:'

		this.lastVisitBox.classList.add('modal-new-element-last-visit-box')
		this.lastVisitBox.append(this.lastVisitText, this.lastVisitDateInput)

		this.infoBox.append(...base, this.lastVisitBox)
	}
	renderCardiologistInputs() {
		const base = this.renderBaseInputs()

		this.pressureInput.setAttribute('type', 'text')
		this.pressureInput.setAttribute('id', `blood-pressure-${this.id}`)
		this.pressureInput.setAttribute('placeholder', 'Blood Pressure')

		this.weightInput.setAttribute('type', 'text')
		this.weightInput.setAttribute('id', `weight-${this.id}`)
		this.weightInput.setAttribute('placeholder', 'Weight')

		this.heartDiseaseInput.setAttribute('type', 'text')
		this.heartDiseaseInput.setAttribute('id', `heart-disease-${this.id}`)
		this.heartDiseaseInput.setAttribute('placeholder', 'Heart Disease')

		this.ageInput.setAttribute('type', 'text')
		this.ageInput.setAttribute('id', `age-${this.id}`)
		this.ageInput.setAttribute('placeholder', 'Age')

		this.infoBox.append(
			...base,
			this.pressureInput,
			this.weightInput,
			this.heartDiseaseInput,
			this.ageInput
		)
	}
	renderBaseInputs() {
		this.infoBox.classList.add('modal-new-element-info-box')
		this.selectPriorityBox.classList.add(
			'modal-new-element-select-priority-box'
		)
		this.selectText.classList.add('modal-new-element-select-text')
		this.selectText.textContent = 'Select priority:'
		this.selectPriorityInput.classList.add('modal-new-element-select-priority')
		this.selectPriorityInput.setAttribute('id', `priority-${this.id}`)
		this.selectPriorityBox.append(this.selectText, this.selectPriorityInput)
		this.selectStatusBox.classList.add('modal-new-element-select-status-box')
		this.selectStatusText.classList.add('modal-new-element-select-text')
		this.selectStatusText.textContent = 'Select status:'
		this.selectStatusInput.classList.add('modal-new-element-select-status')
		this.selectStatusBox.append(this.selectStatusText, this.selectStatusInput)

		if (this.selectStatusInput.hasChildNodes()) {
			this.selectStatusInput.innerHTML = ''
		}
		this.statuses.forEach(status => {
			const option = document.createElement('option')
			option.setAttribute('value', status)
			option.textContent = status
			this.selectStatusInput.append(option)
		})

		this.purposeInput.setAttribute('type', 'text')
		this.purposeInput.setAttribute('id', `purpose-${this.id}`)
		this.purposeInput.setAttribute('placeholder', 'Purpose')

		this.fullNameInput.setAttribute('type', 'text')
		this.fullNameInput.setAttribute('id', `full-name-${this.id}`)
		this.fullNameInput.setAttribute('placeholder', 'Full Name')

		this.descriptionInput.setAttribute('id', `description-${this.id}`)
		this.descriptionInput.setAttribute('placeholder', 'Description')
		this.descriptionInput.style.resize = 'none'
		this.descriptionInput.style.width = '100%'
		this.descriptionInput.style.height = '100px'

		this.selectPriorityInput.setAttribute('id', `priority-${this.id}`)

		if (this.selectPriorityInput.hasChildNodes()) {
			this.selectPriorityInput.innerHTML = ''
		}
		const regularOption = document.createElement('option')
		regularOption.setAttribute('value', 'Regular')
		regularOption.textContent = 'Regular'
		this.selectPriorityInput.appendChild(regularOption)

		const priorityOption = document.createElement('option')
		priorityOption.setAttribute('value', 'Priority')
		priorityOption.textContent = 'Priority'
		this.selectPriorityInput.appendChild(priorityOption)

		const emergencyOption = document.createElement('option')
		emergencyOption.setAttribute('value', 'Emergency')
		emergencyOption.textContent = 'Emergency'
		this.selectPriorityInput.appendChild(emergencyOption)

		return [
			this.fullNameInput,
			this.purposeInput,
			this.descriptionInput,
			this.selectPriorityBox,
			this.selectStatusBox,
		]
	}
	closeModal() {
		this.modalOverlay.remove()
	}
	addEventListeners() {
		this.modalOverlay.addEventListener('click', e => {
			if (e.target.classList.contains('modal-overlay')) {
				this.closeModal()
			}
		})
		this.submitBtn.addEventListener('click', e => {
			e.preventDefault()
			if (this.validate() === false) {
				return
			}
			this.submitBtn.remove()
			this.spinner.classList.add('spinner--active')
			this.modalForm.append(this.spinner)
			this.editVisit()
		})
	}
	editVisit = async () => {
		const msg = await putCard(this.id, {
			doctor: this.doctor,
			fullName: this.fullNameInput.value,
			description: this.descriptionInput.value
				? this.descriptionInput.value
				: '-',
			purpose: this.purposeInput.value ? this.purposeInput.value : '-',
			priority: this.selectPriorityInput.value,
			pressure: this.pressureInput?.value ? this.pressureInput.value : '-',
			weight: this.weightInput?.value ? this.weightInput.value : '-',
			heartDisease: this.heartDiseaseInput?.value
				? this.heartDiseaseInput.value
				: '-',
			age: this.ageInput?.value ? this.ageInput.value : '-',
			lastVisitDate: this.lastVisitDateInput?.value
				? this.lastVisitDateInput.value
				: null,
			cardId: this.cardId ? this.cardId : null,
			status: this.selectStatusInput.value,
		})

		if (msg.status === 200) {
			const cards = await getCards()
			this.statusSelect.value = 'all'
			this.prioritySelect.value = 'all'
			this.cardContainer.innerHTML = ''
			renderCards(this.cardContainer, cards)
		}
		this.closeModal()
	}
	fillInputs() {
		this.fullNameInput.value = this.fullName
		this.purposeInput.value = this.purpose
		this.descriptionInput.value = this.description
		this.selectPriorityInput.value = this.priority
		if (this.doctor === 'Cardiologist') {
			this.pressureInput.value = this.pressure
			this.weightInput.value = this.weight
			this.heartDiseaseInput.value = this.heartDisease
			this.ageInput.value = this.age
		}
		if (this.doctor === 'Dentist') {
			this.lastVisitDateInput.value = this.lastVisitDate
		}
		if (this.doctor === 'Therapist') {
			this.ageInput.value = this.age
		}
	}
	renderInputs() {
		if (this.doctor === 'Cardiologist') {
			this.renderCardiologistInputs()
		}
		if (this.doctor === 'Dentist') {
			this.renderDentistInputs()
		}
		if (this.doctor === 'Therapist') {
			this.renderTherapistInputs()
		}
	}
	render() {
		this.renderInputs()
		this.createElements()
		this.fillInputs()
		this.addEventListeners()
	}
	validate() {
		const letters = /^[a-zA-Zа-яА-ЯіІїЇєЄґҐ]+$/
		const numbers = /^[\d-]+$/
		const fullName = this.fullNameInput.value.trim()
		const purpose = this.purposeInput.value.trim()
		const pressure = this.pressureInput.value
		const weight = this.weightInput.value
		const age = this.ageInput.value

		const messageName = document.createElement('span')
		const messagePurpose = document.createElement('span')
		const messagePressure = document.createElement('span')
		const messageWeight = document.createElement('span')
		const messageAge = document.createElement('span')

		document.querySelectorAll('.invalid-input-message').forEach(el => {
			el.classList.add('none')
		})

		messageName.classList.add('invalid-input-message')
		messagePurpose.classList.add('invalid-input-message')
		messagePressure.classList.add('invalid-input-message')
		messageWeight.classList.add('invalid-input-message')
		messageAge.classList.add('invalid-input-message')

		this.fullNameInput.classList.remove('invalid-input')
		this.purposeInput.classList.remove('invalid-input')
		this.pressureInput.classList.remove('invalid-input')
		this.weightInput.classList.remove('invalid-input')
		this.ageInput.classList.remove('invalid-input')

		if (!letters.test(fullName)) {
			this.fullNameInput.classList.add('invalid-input')
			messageName.innerText = 'Name must contain only letters'
			this.fullNameInput.insertAdjacentElement('beforebegin', messageName)
		}

		if (fullName.length < 2) {
			this.fullNameInput.classList.add('invalid-input')
			messageName.innerText = 'Name must be at least 2 characters long'
			this.fullNameInput.insertAdjacentElement('beforebegin', messageName)
		}

		if (purpose.length < 5) {
			this.purposeInput.classList.add('invalid-input')
			messagePurpose.innerText = 'Purpose must be at least 5 characters long'
			this.purposeInput.insertAdjacentElement('beforebegin', messagePurpose)
		}

		if (!numbers.test(pressure)) {
			this.pressureInput.classList.add('invalid-input')
			messagePressure.innerText =
				'Please input blood pressure in format "120-90"'
			this.pressureInput.insertAdjacentElement('beforebegin', messagePressure)
		}

		if ((weight != '-' && 2 > weight) || weight > 250 || isNaN(weight)) {
			if (weight === '-') return

			this.weightInput.classList.add('invalid-input')
			messageWeight.innerText = 'Please input correct weight'
			this.weightInput.insertAdjacentElement('beforebegin', messageWeight)
		}

		if ((age != '-' && 0 > age) || age > 100 || isNaN(age)) {
			if (age === '-') return
			this.ageInput.classList.add('invalid-input')
			messageAge.innerText = 'Please input correct age'
			this.ageInput.insertAdjacentElement('beforebegin', messageAge)
		}

		if (document.querySelector('.invalid-input')) {
			return false
		}
	}
}

// import { main } from '../constants/mainConstants.js'
// import putCard from '../api/putCard.js'
// import renderCards from '../utils/renderCards.js'
// import NewElementForm from './NewElementForm.js'
// export default class EditForm extends NewElementForm {
// 	constructor(
// 		doctor,
// 		fullName,
// 		purpose,
// 		description,
// 		priority,
// 		pressure,
// 		weight,
// 		heartDisease,
// 		age,
// 		lastVisitDate,
// 		cardId,
// 		id,
// 		status = 'Open'
// 	) {
// 		super()
// 		this.modalOverlay = document.createElement('div')
// 		this.modalContainer = document.createElement('div')
// 		this.modalTitle = document.createElement('h2')
// 		this.modalForm = document.createElement('form')
// 		this.doctor = doctor
// 		this.fullName = fullName
// 		this.purpose = purpose
// 		this.description = description
// 		this.priority = priority
// 		this.pressure = pressure
// 		this.weight = weight
// 		this.heartDisease = heartDisease
// 		this.age = age
// 		this.lastVisitDate = lastVisitDate
// 		this.cardId = cardId
// 		this.id = id
// 		this.status = status
// 		this.submitBtn = document.createElement('button')
// 		this.selectStatusInput = document.createElement('select')
// 		this.selectStatusText = document.createElement('p')
// 		this.selectStatusBox = document.createElement('div')
// 		this.statuses = ['Open', 'Done']
// 	}

// 	createElements() {
// 		super.createElements()
// 		this.modalContainer.classList.add('modal-new-element-container')
// 		this.modalOverlay.classList.add('modal-overlay')
// 		this.modalTitle.classList.add('modal-new-element-title')
// 		this.modalForm.classList.add('modal-new-element-form')
// 		this.selectDoctor.classList.add('modal-new-element-select')
// 		this.doctors.forEach(doctor => {
// 			const option = document.createElement('option')
// 			option.setAttribute('value', doctor)
// 			option.textContent = doctor
// 			this.selectDoctor.append(option)
// 		})

// 		this.submitBtn.classList.add('modal-new-element-submit-btn')
// 		this.modalTitle.textContent = 'New Visit'
// 		this.submitBtn.textContent = 'Create'
// 		this.modalForm.append(this.selectDoctor, this.infoBox, this.submitBtn)
// 		this.modalContainer.append(this.modalTitle, this.modalForm)
// 		this.modalOverlay.append(this.modalContainer)
// 		main.append(this.modalOverlay)
// 	}

// 	renderTherapistInputs() {
// 		const base = this.renderBaseInputs()
// 		this.age.setAttribute('type', 'text')
// 		this.age.setAttribute('id', 'age')
// 		this.age.setAttribute('placeholder', 'Age')
// 		this.infoBox.append(...base, this.age)
// 	}

// 	renderDentistInputs() {
// 		const base = this.renderBaseInputs()
// 		this.lastVisitDate.setAttribute('type', 'date')
// 		this.lastVisitDate.setAttribute('id', 'last-visit-date')
// 		this.lastVisitDate.setAttribute('lang', 'en')

// 		this.lastVisitText.classList.add('modal-new-element-last-visit-text')
// 		this.lastVisitText.textContent = 'Last Visit:'

// 		this.lastVisitBox.classList.add('modal-new-element-last-visit-box')
// 		this.lastVisitBox.append(this.lastVisitText, this.lastVisitDate)

// 		this.infoBox.append(...base, this.lastVisitBox)
// 	}

// 	renderCardiologistInputs() {
// 		const base = this.renderBaseInputs()

// 		this.pressure.setAttribute('type', 'text')
// 		this.pressure.setAttribute('id', 'blood-pressure')
// 		this.pressure.setAttribute('placeholder', 'Blood Pressure')

// 		this.weight.setAttribute('type', 'text')
// 		this.weight.setAttribute('id', 'weight')
// 		this.weight.setAttribute('placeholder', 'Weight')

// 		this.heartDisease.setAttribute('type', 'text')
// 		this.heartDisease.setAttribute('id', 'heart-disease')
// 		this.heartDisease.setAttribute('placeholder', 'Heart Disease')

// 		this.age.setAttribute('type', 'text')
// 		this.age.setAttribute('id', 'age')
// 		this.age.setAttribute('placeholder', 'Age')

// 		this.infoBox.append(
// 			...base,
// 			this.pressure,
// 			this.weight,
// 			this.heartDisease,
// 			this.age
// 		)
// 	}

// 	renderBaseInputs() {
// 		this.infoBox.classList.add('modal-new-element-info-box')
// 		this.selectPriorityBox.classList.add(
// 			'modal-new-element-select-priority-box'
// 		)
// 		this.selectText.classList.add('modal-new-element-select-text')
// 		this.selectText.textContent = 'Select priority:'
// 		this.selectPriority.classList.add('modal-new-element-select-priority')
// 		this.selectPriority.setAttribute('id', 'priority')
// 		this.selectPriorityBox.append(this.selectText, this.selectPriority)

// 		this.purpose.setAttribute('type', 'text')
// 		this.purpose.setAttribute('id', 'purpose')
// 		this.purpose.setAttribute('placeholder', 'Purpose')
// 		this.purpose.setAttribute('required', '')

// 		this.fullName.setAttribute('type', 'text')
// 		this.fullName.setAttribute('id', 'full-name')
// 		this.fullName.setAttribute('placeholder', 'Full Name')
// 		this.fullName.setAttribute('required', '')

// 		this.description = document.createElement('textarea')
// 		this.description.setAttribute('id', 'description')
// 		this.description.setAttribute('placeholder', 'Description')
// 		this.description.style.resize = 'none'
// 		this.description.style.width = '100%'
// 		this.description.style.height = '100px'

// 		this.selectPriority.setAttribute('id', 'priority')

// 		if (this.selectPriority.hasChildNodes()) {
// 			this.selectPriority.innerHTML = ''
// 		}
// 		const regularOption = document.createElement('option')
// 		regularOption.setAttribute('value', 'Regular')
// 		regularOption.textContent = 'Regular'
// 		this.selectPriority.appendChild(regularOption)

// 		const priorityOption = document.createElement('option')
// 		priorityOption.setAttribute('value', 'Priority')
// 		priorityOption.textContent = 'Priority'
// 		this.selectPriority.appendChild(priorityOption)

// 		const emergencyOption = document.createElement('option')
// 		emergencyOption.setAttribute('value', 'Emergency')
// 		emergencyOption.textContent = 'Emergency'
// 		this.selectPriority.appendChild(emergencyOption)

// 		return [
// 			this.fullName,
// 			this.purpose,
// 			this.description,
// 			this.selectPriorityBox,
// 		]
// 	}

// 	getFormValues() {
// 		const obj = {
// 			doctor: this.selectDoctor.value,
// 			fullName: this.fullName.value,
// 			description: this.description.value ? this.description.value : '-',
// 			purpose: this.purpose.value ? this.purpose.value : '-',
// 			priority: this.selectPriority.value,
// 			pressure: this.pressure.value ? this.pressure.value : '-',
// 			weight: this.weight.value ? this.weight.value : '-',
// 			heartDisease: this.heartDisease.value ? this.heartDisease.value : '-',
// 			age: this.age.value ? this.age.value : '-',
// 			lastVisitDate: this.lastVisitDate.value ? this.lastVisitDate.value : '-',
// 			cardId: this.cardId ? this.cardId : null,
// 		}
// 		return obj
// 	}
// 	addEventListeners() {
// 		super.addEventListeners()
// 		this.selectDoctor.addEventListener('change', e => {
// 			this.infoBox.innerHTML = ''
// 			this.renderInputs()
// 		})
// 		this.submitBtn.addEventListener('click', e => {
// 			e.preventDefault()
// 			if (this.validate() === false) {
// 				return
// 			}
// 			this.submitBtn.remove()
// 			this.spinner.classList.add('spinner--active')
// 			this.modalForm.append(this.spinner)
// 			this.editVisit()
// 		})
// 	}

// 	renderInputs() {
// 		super.renderInputs()
// 		if (this.selectDoctor.value === 'Cardiologist') {
// 			this.renderCardiologistInputs()
// 		}
// 		if (this.selectDoctor.value === 'Dentist') {
// 			this.renderDentistInputs()
// 		}
// 		if (this.selectDoctor.value === 'Therapist') {
// 			this.renderTherapistInputs()
// 		}
// 	}
// 	editVisit = async () => {
// 		const msg = await putCard(this.id, {
// 			doctor: this.doctor,
// 			fullName: this.fullName.value,
// 			description: this.description.value ? this.description.value : '-',
// 			purpose: this.purpose.value ? this.purpose.value : '-',
// 			priority: this.selectPriority.value,
// 			pressure: this.pressure?.value ? this.pressure.value : '-',
// 			weight: this.weight?.value ? this.weight.value : '-',
// 			heartDisease: this.heartDisease?.value ? this.heartDisease.value : '-',
// 			age: this.age?.value ? this.age.value : '-',
// 			lastVisitDate: this.lastVisitDate?.value
// 				? this.lastVisitDate.value
// 				: null,
// 			cardId: this.cardId ? this.cardId : null,
// 			status: this.selectStatusInput.value,
// 		})
// 		if (msg.status === 200) {
// 			this.cardContainer.innerHTML = ''
// 			renderCards()
// 		}
// 		this.closeModal()
// 	}
// 	validate() {
// 		super.validate()
// 		const letters = /^[a-zA-Zа-яА-ЯіІїЇєЄґҐ]+$/
// 		const fullName = this.fullName.value.trim()
// 		const purpose = this.purpose.value.trim()
// 		const pressure = this.pressure.value
// 		const weight = this.weight.value
// 		const age = this.age.value

// 		const messageName = document.createElement('span')
// 		const messagePurpose = document.createElement('span')
// 		const messagePressure = document.createElement('span')
// 		const messageWeight = document.createElement('span')
// 		const messageAge = document.createElement('span')

// 		document.querySelectorAll('.invalid-input-message').forEach(el => {
// 			el.classList.add('none')
// 		})

// 		messageName.classList.add('invalid-input-message')
// 		messagePurpose.classList.add('invalid-input-message')
// 		messagePressure.classList.add('invalid-input-message')
// 		messageWeight.classList.add('invalid-input-message')
// 		messageAge.classList.add('invalid-input-message')

// 		this.fullName.classList.remove('invalid-input')
// 		this.purpose.classList.remove('invalid-input')
// 		this.pressure.classList.remove('invalid-input')
// 		this.weight.classList.remove('invalid-input')
// 		this.age.classList.remove('invalid-input')

// 		if (!letters.test(fullName)) {
// 			this.fullName.classList.add('invalid-input')
// 			messageName.innerText = 'Name must contain only letters'
// 			this.fullName.insertAdjacentElement('beforebegin', messageName)
// 		}

// 		if (fullName.length < 2) {
// 			this.fullName.classList.add('invalid-input')
// 			messageName.innerText = 'Name must be at least 2 characters long'
// 			this.fullName.insertAdjacentElement('beforebegin', messageName)
// 		}

// 		if (purpose.length < 5) {
// 			this.purpose.classList.add('invalid-input')
// 			messagePurpose.innerText = 'Purpose must be at least 5 characters long'
// 			this.purpose.insertAdjacentElement('beforebegin', messagePurpose)
// 		}

// 		if (
// 			(pressure != '' && 50 > pressure) ||
// 			pressure > 250 ||
// 			isNaN(pressure)
// 		) {
// 			this.pressure.classList.add('invalid-input')
// 			messagePressure.innerText = 'Please input correct pressure'
// 			this.pressure.insertAdjacentElement('beforebegin', messagePressure)
// 		}

// 		if ((weight != '' && 2 > weight) || weight > 250 || isNaN(weight)) {
// 			this.weight.classList.add('invalid-input')
// 			messageWeight.innerText = 'Please input correct weight'
// 			this.weight.insertAdjacentElement('beforebegin', messageWeight)
// 		}

// 		if ((age != '' && 0 > age) || age > 100 || isNaN(age)) {
// 			this.age.classList.add('invalid-input')
// 			messageAge.innerText = 'Please input correct age'
// 			this.age.insertAdjacentElement('beforebegin', messageAge)
// 		}

// 		if (document.querySelector('.invalid-input')) {
// 			return false
// 		}
// 	}
// }
