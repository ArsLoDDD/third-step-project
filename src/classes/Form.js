export default class Form {
	constructor() {
		this.modalOverlay = document.createElement('div')
		this.modalContainer = document.createElement('div')
		this.modalTitle = document.createElement('h2')
		this.modalForm = document.createElement('form')
	}
	createElements() {}
	closeModal() {
		this.modalOverlay.remove()
	}
	addEventListeners() {
		this.modalOverlay.addEventListener('click', e => {
			if (e.target.classList.contains('modal-overlay')) {
				this.closeModal()
			}
		})
	}
	render() {
		this.createElements()
		this.addEventListeners()
	}
}
