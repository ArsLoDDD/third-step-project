import Visit from './Visit.js'

class VisitDentist extends Visit {
	constructor(
		edit,
		appendElement,
		doctor,
		fullName,
		purpose,
		description,
		priority,
		pressure = null,
		weight = null,
		heartDisease = null,
		age = null,
		lastVisitDate,
		cardId,
		id,
		status
	) {
		super(
			edit,
			appendElement,
			doctor,
			fullName,
			purpose,
			description,
			priority,
			pressure,
			weight,
			heartDisease,
			age,
			lastVisitDate,
			cardId,
			id,
			status
		)
	}
	createElements() {
		super.createElements()
		this.specificInfo.innerHTML = `<p class="additional-info__last-visit" id="client-last-visit"> Last Visit: ${this.lastVisitDate}</p>`
	}
}

export default VisitDentist
