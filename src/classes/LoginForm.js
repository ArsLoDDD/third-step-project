import {
	authorization,
	loginBtnContent,
	passwordPlaceholder,
	emailPlaceholder,
} from '../constants/langContsEng.js'
import { main } from '../constants/mainConstants.js'
import loginFun from '../api/loginFun.js'

import Form from './Form.js'
import renderCards from '../utils/renderCards.js'
import getCards from '../api/getCards.js'
import OnLogBtns from './OnLogBtns.js'
export default class LoginForm extends Form {
	constructor(modalOverlay, modalContainer, modalTitle, modalForm) {
		super(modalOverlay, modalContainer, modalTitle, modalForm)

		this.email = document.createElement('input')
		this.password = document.createElement('input')
		this.submitBtn = document.createElement('button')
		this.signIn = document.querySelector('.signIn')
		this.signUp = document.querySelector('.signUp')
		this.searchContainer = document.querySelector('.search-container')
	}
	createElements() {
		super.createElements()
		this.modalOverlay.classList.add('modal-overlay')
		this.modalContainer.classList.add('modal-container')
		this.modalTitle.classList.add('modal-title')
		this.modalForm.classList.add('modal-form')
		this.submitBtn.classList.add('modal-submit-btn')
		this.email.setAttribute('type', 'email')
		this.email.setAttribute('id', 'email')
		this.email.setAttribute('name', 'email')
		this.email.setAttribute('placeholder', emailPlaceholder)
		// this.email.setAttribute('autocomplete', 'email')
		this.password.setAttribute('type', 'password')
		this.password.setAttribute('id', 'password')
		this.password.setAttribute('name', 'password')
		this.password.setAttribute('placeholder', passwordPlaceholder)
		// this.password.setAttribute('autocomplete', 'current-password')
		this.submitBtn.setAttribute('type', 'submit')
		this.submitBtn.textContent = loginBtnContent
		this.modalTitle.textContent = authorization
		this.modalForm.append(this.email, this.password, this.submitBtn)
		this.modalContainer.append(this.modalTitle, this.modalForm)
		this.modalOverlay.append(this.modalContainer)
		main.append(this.modalOverlay)
	}
	addEventListeners() {
		super.addEventListeners()
		this.modalForm.addEventListener('submit', async e => {
			e.preventDefault()
			const loginBool = await loginFun({
				email: e.target.children[0].value,
				password: e.target.children[1].value,
			})
			if (loginBool) {
				const cards = await getCards()
				renderCards(undefined, cards)
				new OnLogBtns().render()
				this.signIn.classList.add('none')
				this.signUp.classList.add('none')
				this.searchContainer.style.display = 'flex'
				this.closeModal()
			}
		})
	}
}
