import Form from './Form.js'

import { main } from '../constants/mainConstants.js'
export default class SignUp extends Form {
	constructor(modalOverlay, modalContainer, modalTitle, modalForm) {
		super(modalOverlay, modalContainer, modalTitle, modalForm)

		this.email = document.createElement('input')
		this.password = document.createElement('input')
		this.submitBtn = document.createElement('button')
		this.signUp = document.querySelector('.signUp')
		this.searchContainer = document.querySelector('.search-container')
	}
	createElements() {
		super.createElements()
		this.modalOverlay.classList.add('modal-overlay')
		this.modalContainer.classList.add('modal-container')
		this.modalTitle.classList.add('modal-title')
		this.modalForm.classList.add('modal-form')
		this.submitBtn.classList.add('modal-submit-btn')
		this.email.setAttribute('type', 'email')
		this.email.setAttribute('id', 'email')
		this.email.setAttribute('name', 'email')
		this.email.setAttribute('placeholder', 'Email')
		// this.email.setAttribute('autocomplete', 'email')
		this.password.setAttribute('type', 'password')
		this.password.setAttribute('id', 'password')
		this.password.setAttribute('name', 'password')
		this.password.setAttribute('placeholder', 'Password')
		this.submitBtn.setAttribute('type', 'submit')
		this.submitBtn.textContent = 'Sign Up'
		this.modalTitle.textContent = 'Sign Up'
		this.modalForm.append(this.email, this.password, this.submitBtn)
		this.modalContainer.append(this.modalTitle, this.modalForm)
		this.modalOverlay.append(this.modalContainer)
		main.append(this.modalOverlay)
	}
	addEventListeners() {
		super.addEventListeners()
		this.modalForm.addEventListener('submit', async e => {
			e.preventDefault()
			const registration = await axios.post(
				'https://ajax.test-danit.com/api/cards/register',
				{
					email: this.email.value,
					password: this.password.value,
				}
			)
			if (registration.status === 200) {
				this.closeModal()
			}
		})
	}
}
