export default class Empty {
	constructor() {
		this.container = document.querySelector('.card__container')
		this.empty = document.createElement('div')
		this.text = document.createElement('p')
	}
	createElements() {
		this.empty.classList.add('item-have-been')
		this.text.textContent = 'No items have been added'
		this.empty.append(this.text)
		this.container.append(this.empty)
	}
	render() {
		this.createElements()
	}
}
